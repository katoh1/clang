//
//  AppDelegate.h
//  ClangTester
//
//  Created by dawid on 05/01/17.
//  Copyright © 2017 dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate:UIResponder< UIApplicationDelegate,UITableViewDelegate >

@property (strong, nonatomic) UIWindow *window;


@end

