//
//  main.m
//  ClangTester
//
//  Created by dawid on 05/01/17.
//  Copyright © 2017 dawid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
